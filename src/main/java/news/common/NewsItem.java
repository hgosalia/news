package news.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by hiralgosalia on 2/8/16.
 */
public class NewsItem implements Serializable {

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("date")
    @Expose
    private Date articleDate;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("wire")
    @Expose
    private String wire;

    public NewsItem(String title, String url, Date date, String category, String wire) {
        super();
        this.title = title;
        this.url = url;
        this.articleDate = date;
        this.category = category;
        this.wire = wire;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public Date getArticleDate() {
        return articleDate;
    }

    public String getCategory() {
        return category;
    }

    public String getWire() {
        return wire;
    }

}
