package news.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by hiralgosalia on 2/7/16.
 */
public class GsonFactoryBean {

    public static Gson create() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }
}
