package news.service;

import com.google.gson.Gson;
import news.common.NewsItem;
import news.dao.NewsDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by hiralgosalia on 2/8/16.
 */

@Path("/news")
public class NewsService {

    private NewsDAO newsDAO;
    private Gson gson;

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    @Path("/latest")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLatestNewsHeadlines() {

        //call the DAO to get the latest news items
        List<NewsItem> newsItems = newsDAO.getLatestNews();

        //convert the news items retrieved to a JSON string
        String jsonResponse = gson.toJson(newsItems);

        return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
    }


}
