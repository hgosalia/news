package news.dao;

import news.common.NewsItem;

import java.util.List;

/**
 * Created by hiralgosalia on 2/8/16.
 */
public interface NewsDAO {

    public List<NewsItem> getLatestNews();

    public String getName();
}
