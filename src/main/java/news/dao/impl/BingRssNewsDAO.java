package news.dao.impl;

import com.rometools.rome.feed.synd.SyndEntry;
import org.jdom2.Element;

/**
 * Created by hiralgosalia on 2/10/16.
 */
public class BingRssNewsDAO extends RssNewsDAO {

    @Override
    protected String extractUrl(SyndEntry u) {
        String url = u.getLink();
        int urlIdx = url.indexOf("url=");
        if (urlIdx > 0) {
            int end = url.indexOf("&amp;",urlIdx);
            if (end > urlIdx) {
                return url.substring(urlIdx + "url=".length(), end);
            } else {
                return url.substring(urlIdx + "url=".length());
            }
        } else {
            return super.extractUrl(u);
        }
    }

    @Override
    protected String extractWire(SyndEntry w) {

        for (Element e : w.getForeignMarkup()) {
            if ("Source".equals(e.getName()) || "News:Source".equals(e.getName())) {
                return e.getValue();
            }
        }
        return super.extractWire(w);
    }
}
