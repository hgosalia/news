package news.dao.impl;

import news.common.NewsItem;
import news.dao.NewsDAO;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Created by hiralgosalia on 2/8/16.
 */
public class GuardianNewsDAO implements NewsDAO {

    private static final Logger log = LoggerFactory.getLogger(GuardianNewsDAO.class);

    private OkHttpClient httpClient;
    private String url;
    private String name;

    public void setHttpClient(OkHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public List<NewsItem> getLatestNews() {
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = httpClient.newCall(request).execute();
            String responseBody = response.body().string();
            log.info(responseBody);
        } catch (IOException e) {
            log.error("Error retrieving latest news from Guardian - {}",e.getMessage());
        }
        return null;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
