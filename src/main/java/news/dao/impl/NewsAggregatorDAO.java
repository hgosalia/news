package news.dao.impl;

import news.common.NewsItem;
import news.dao.NewsDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Hiral Gosalia on 12/9/16.
 */
public class NewsAggregatorDAO implements NewsDAO {

    private static final Logger log = LoggerFactory.getLogger(NewsAggregatorDAO.class);

    private Set<NewsDAO> newsDAOSet;
    private String name;

    public void setNewsDAOSet(Set<NewsDAO> newsDAOSet) {
        this.newsDAOSet = newsDAOSet;
    }

    @Override
    public List<NewsItem> getLatestNews() {

        List<NewsItem> newsItems = new ArrayList<>();
        for (NewsDAO newsDAO : newsDAOSet) {
            try {
                List<NewsItem> items = newsDAO.getLatestNews();
                log.info( "Retrieved {} results from {}", items==null ? 0 : items.size(), newsDAO.getName());
                newsItems.addAll(newsDAO.getLatestNews());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return newsItems;
    }

    public void setName(String name) { this.name = name; }

    public String getName() { return this.name; }
}
