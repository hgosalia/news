package news.dao.impl;

import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import news.common.NewsItem;
import news.dao.NewsDAO;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by hiralgosalia on 2/10/16.
 */
public abstract class RssNewsDAO implements NewsDAO {

    private static final Logger log = LoggerFactory.getLogger(RssNewsDAO.class);

    private OkHttpClient httpClient;
    private String rssUrl;
    private String name;

    public void setName(String name) { this.name = name; }

    public String getName() { return this.name; }

    public void setHttpClient(OkHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public void setRssUrl(String url) {
        this.rssUrl = url;
    }

    protected String extractTitle(SyndEntry s) {
        return s.getTitle();
    }

    protected String extractUrl(SyndEntry u) {
        return u.getLink();
    }

    protected Date extractDate(SyndEntry d) {
        return d.getPublishedDate();
    }

    protected String extractWire(SyndEntry w) {
        return (String)w.getWireEntry();
    }

    protected String extractCategory(SyndEntry c) {
        List<SyndCategory> categories = c.getCategories();
        if (CollectionUtils.isEmpty(categories)) {
            return null;
        } else {
            SyndCategory syndCategory = categories.get(0);
            return syndCategory.getName();
        }
    }

    @Override
    public List<NewsItem> getLatestNews() {
        List<NewsItem> newsItems = null;

        try {
            InputStream responseBodyStream = getResponseBody();

            newsItems = buildResults(responseBodyStream);

        } catch (IOException |FeedException e) {
            log.error("Error retrieving latest news from {} - {}", getName(), e.getMessage());
        }
        return newsItems;
    }

    protected List<NewsItem> buildResults(InputStream responseBodyStream) throws IOException, FeedException {

        //InputStream is = IOUtils.toInputStream(responseBody, "UTF-8");
        SyndFeedInput input = new SyndFeedInput();
        SyndFeed sf = input.build(new XmlReader(responseBodyStream));
        List<SyndEntry> entries = sf.getEntries();
        Iterator<SyndEntry> it = entries.iterator();

        List<NewsItem> newsItems = new ArrayList<>(entries.size());
        while (it.hasNext()) {
            SyndEntry entry = it.next();

            String title = extractTitle(entry);
            String link = extractUrl(entry);
            Date date = extractDate(entry);
            String wire =extractWire(entry);
            String category = extractCategory(entry);

            link = java.net.URLDecoder.decode(link, "UTF-8");
            NewsItem newsItem = new NewsItem(
                    title,
                    link,
                    date,
                    category,
                    wire);

            newsItems.add(newsItem);
        }
        return newsItems;
    }

    protected InputStream getResponseBody() throws IOException {
        Request request = new Request.Builder()
                .url(rssUrl)
                .build();

        Response response = httpClient.newCall(request).execute();
        return response.body().byteStream();
    }
}
