package news.dao.impl;

import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import news.common.NewsItem;
import news.dao.NewsDAO;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by hiralgosalia on 2/8/16.
 */
public class GoogleRssNewsDAO extends RssNewsDAO {

    private static final Logger log = LoggerFactory.getLogger(GoogleRssNewsDAO.class);

    @Override
    protected String extractTitle(SyndEntry s) {
        String title = s.getTitle();
        int hyphenIdx = title.indexOf('-');
        if (hyphenIdx > 0) {
            title = title.substring(0, hyphenIdx-1).trim();
            return title;
        } else {
            return super.extractTitle(s);
        }
    }

    @Override
    protected String extractWire(SyndEntry u) {
        //it looks as if the wire/source is in the title
        //after the hyphen
        String title = u.getTitle();
        int hyphenIdx = title.indexOf('-');
        if (hyphenIdx > 0) {
            return title.substring(hyphenIdx+1).trim();
        } else {
            return super.extractWire(u);
        }
    }

    @Override
    protected String extractUrl(SyndEntry u) {
        String url = u.getLink();
        int urlIdx = url.indexOf("&url=");
        if (urlIdx > 0) {
            return url.substring(urlIdx+ "&url=".length());
        } else {
            return super.extractUrl(u);
        }
    }
}
